/*
Error in the B module: dropping the same nonterminal twice
*/

module A {
    a -> 'a';
}

module B {
    import A
        drop a
        drop a;
    a -> '1';
    b -> a;
}
