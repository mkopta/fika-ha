/*
Error in the B module: rename of a nonexisting nonterminal
*/

module A {
    a -> 'a';
    y -> 'y';
}

module B {
    import A rename x as y;
    b -> a | y;
    abstract a;
    abstract y;
}
