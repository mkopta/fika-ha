/*
Error in the D module: A is imported twice - first from B, second from C
*/

module A {
    a -> 'a';
}

module B {
    import A;
}

module C {
    import A;
}

module D {
    import B;
    import C;
    d -> a;
    abstract a;
}
