/*
Result:
    x1 -> '1';
    x2 -> '2';
    c -> x1 | x2;
*/

module A {
    a -> '1';
    b -> '2';
    c -> '3';
}

module X {
    import A
        rename a as x1
        rename b as x2
        drop c;
    override c -> x1 | x2;
}
