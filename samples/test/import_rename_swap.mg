/*
A swap of the names in an import should not cause any conflict.
*/

module A {
    x -> '1';
    y -> '2';
}

module B {
    import A
        rename x as y
        rename y as x;
    b -> x | y;
    abstract x;
    abstract y;
}
