/*
Error in the B module: dropping of a nonexisting nonterminal
*/

module A {
    a -> 'a';
}

module B {
    import A drop x;
    x -> 'y';
    b -> a | x;
    abstract a;
    abstract x;
}
