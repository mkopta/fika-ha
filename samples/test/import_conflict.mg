/*
Error in the C module: import of a conflicting nonterminal (should be renamed)
*/

module A {
    x -> '1';
    abstract x;
}

module B {
    x -> '2';
    abstract x;
}

module C {
    import A;
    import B;
    c -> x;
    abstract x;
}
