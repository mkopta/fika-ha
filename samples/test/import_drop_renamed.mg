/*
Drop and rename for the same NT in one import. Result?
*/

module A {
    a -> 'a';
}

module B {
    import A
        rename a as x
        drop a;
    override x -> 'x';
}
