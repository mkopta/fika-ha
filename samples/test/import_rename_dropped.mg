/*
Rename and drop for the same NT in one import. Result?
*/

module A {
    a -> 'a';
}

module B {
    import A
        drop a
        rename a as x;
    x -> 'x';
}
