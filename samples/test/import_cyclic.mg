/*
Error in the B module: cyclic import
*/

module A {
    import B;
}

module B {
    import A;
}
