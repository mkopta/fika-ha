/*
Error in the B module: rename cannot replace any other nonterminal in the same
module (even if it has been dropped)
*/

module A {
    x -> '1';
    y -> '2';
}

module B {
    import A
        drop y
        rename x as y;
    b -> y;
    abstract y;
}
