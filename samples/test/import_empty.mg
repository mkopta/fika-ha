/*
Importing an empty module - this should be OK
*/

module Empty {}

module Main {
    import Empty;
    start -> 'a';
}
