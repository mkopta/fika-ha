/*
Importing of many same abstract nonterminals should be OK as well as drop them.
*/

module A {
    abstract x;
}

module B {
    abstract x;
}

module C {
    x -> 'c';
}

module X {
    import A;
    import B;
    import C drop x;
    
    override x -> 'x';
}
