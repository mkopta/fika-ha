/*
Dropping an abstract NT should be OK. It causes nothing.
*/

module A {
    abstract a;
}

module B {
    import A drop a;
    a -> 'a';
    b -> a;
}
