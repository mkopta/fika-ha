/*
Error in the B module: missing the override keyword
*/

module A {
    a -> 'a';
}

module B {
    import A;
    a -> '1';
}
