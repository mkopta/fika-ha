/*
Result:
    abstract operand;
    plus -> '+';
    minus -> '-';

    addition -> operand plus operand;
    subtraction -> operand minus operand;
    expression -> addition | subtraction;
*/

module BinaryOp {
    binaryOp -> 'a' operator 'a';
    abstract operand;
    abstract operator;
}

module Addition {
    import BinaryOp
        rename operator as plus
        rename binaryOp as addition;
    plus -> '+';
}

module Subtraction {
    import BinaryOp
        rename operator as minus
        rename binaryOp as subtraction;
    minus -> '-';
}

module Expression {
    import Addition;
    import Subtraction;
    expression -> addition | subtraction;
    abstract addition;
    abstract subtraction;
}
