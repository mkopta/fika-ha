/*
Result:
    a -> '1';
*/

module A {
    a -> 'a';
}

module B {
    import A;
    override a -> '1';
}
