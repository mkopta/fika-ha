/*
Error in the B module: renaming the same nonterminal twice
*/

module A {
    a -> 'a';
}

module B {
    import A
        rename a as x
        rename a as y;
    b -> x | y;
    abstract x;
    abstract y;
}
