module C {
  Ident -> Letter IdentX;
  IdentX -> Letter IdentX | Number IdentX | Nothing;
  abstract Letter;
  abstract Number;
  abstract Nothing;
}
