module Branching {
  Branching -> 'if' Condition 'then' Command 'else' Command ;
  abstract Condition;
  abstract Command;
}

module Common {
  PlaceHolder -> 'placeholder' ;
  Nil -> ;
}

module Base {
  abstract PlaceHolder;
  Condition -> PlaceHolder;
  Comment -> PlaceHolder;
}

module Commands {
  Commands -> Command Rest ;
  Rest     -> Commands | Nothing ;
  Command  -> Branching | PlaceHolder ;
  abstract Nothing;
  abstract Branching;
  abstract PlaceHolder;
  import Common rename Nil as Nothing;
  import Branching;
  import Base drop Comment;
}

module Program {
  import Commands;
  override Commands -> Program;
}

/*

Expected output grammar:

Program     -> Command Rest ;
Rest        -> Program | Nothing ;
Commands    -> Branching | PlaceHolder ;
PlaceHolder -> 'placeholder' ;
Nothing     -> ;
Branching   -> 'if' Condition 'then' Command 'else' Command ;
Condition   -> PlaceHolder;

*/
