/* Fika v01 input data grammar
   without FIRST-FIRST or FIRST-FOLLOW conflicts
*/

module Head {
  Init -> Module Init_;
  Init_ -> Module Init_ | ;
  import Module rename Mod as Module;
  import Common;
  abstract Module;
  override Arrow -> '->';
}

module Module {
  Mod -> 'module' Modulename '{' Content '}';
  Modulename -> Identificator;
  Content -> | Statement ';' Content;
  Statement -> Rule | Import | Abstract | Override;
  abstract Identificator;
  abstract Rule;
  abstract Import;
  abstract Abstract;
  abstract Override;
  import Rule;
  import Import;
  import Abstract;
  import Override;
}

module Rule {
  Rule -> Nonterminal Arrow Elements;
  Elements -> Element Elements | ;
  Element -> Nonterminal | Terminal;
  Nonterminal -> Identificator;
  Terminal -> String;
  Arrow => '=>' | '->';
  abstract Identificator;
  abstract String;
}

module Import {
  Import -> 'import' Modulename Import_;
  Import_ -> 'rename' Nonterminal 'as' Nonterminal Import_
           | 'drop' Nonterminal Import_
           | ;
  abstract Modulename;
  abstract Nonterminal;
}

module Override {
  Override -> 'override' Rule;
  abstract Rule;
}

module Abstract {
  Abstract -> 'abstract' Nonterminal;
  abstract Nonterminal;
}

module Common {
  Identificator -> '_' Identificator_ | Letter Identificator_;
  Identificator_ -> '_' Identificator_
                  | Letter Identificator_
                  | Number Identificator_
                  | ;
  Letter ->
    'A' | 'a' | 'B' | 'b' | 'C' | 'c' | 'D' | 'd' | 'E' | 'e' |
    'E' | 'e' | 'F' | 'f' | 'G' | 'g' | 'H' | 'h' | 'I' | 'i' |
    'J' | 'j' | 'K' | 'k' | 'L' | 'l' | 'M' | 'm' | 'N' | 'n' |
    'O' | 'o' | 'P' | 'p' | 'Q' | 'q' | 'R' | 'r' | 'S' | 's' |
    'T' | 't' | 'U' | 'u' | 'V' | 'v' | 'W' | 'w' | 'Y' | 'y' |
    'Z' | 'z';
  Number ->
    '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9';
  String -> Letter String | Number String | ;
}

/* eof */
