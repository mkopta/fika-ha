
/* Simple testing grammar */

module E {
  import A;
  import B;
  import C;
  import D;
}

/*
  Number -> '1' | '2' | '3'
  Letter -> 'a' | 'b' | 'c'
  Ident -> Letter IdentX
  IdentX -> Letter IdentX | Number IdentX | Nothing;
  Nothing -> ;
*/
