module OutputProcessing where

import Data

-- TODO
createOutput :: Grammar -> Output
createOutput (Module _ c) = format c

format :: [Statement] -> String
format [] = ""
format ((Rule l r):ss) = formatRule l r ++ "\n" ++ format ss
format (s:ss) = show s ++ "\n" ++ format ss

formatRule :: Element -> [[Element]] -> String
formatRule l r = formatElement l ++ " -> " ++ formatRightsides r

formatRightsides :: [[Element]] -> String
formatRightsides [] = ""
formatRightsides (x:xs) | xs /= [] = formatRightside x ++ "| " ++ formatRightsides xs
                        | otherwise = formatRightside x

formatRightside :: [Element] -> String
formatRightside [] = ""
formatRightside (e:es) = formatElement e ++ " " ++ formatRightside es

formatElement :: Element -> String
formatElement (Nonterminal nt) = nt
formatElement (Terminal t) = "\"" ++ t ++ "\""

-- EOF
