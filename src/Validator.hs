module Validator where

import Data
import Reducer
import List


validateModules :: [Module] -> Bool
validateModules [] = True
validateModules ms = checkForDups ms


checkForDups :: [Module] -> Bool
checkForDups [] = True
checkForDups ((Module m _):ms) =
  if notIn m ms
    then checkForDups ms
    else error ("Error. There are two modules named '" ++ m ++ "'.")
  where
    notIn mname1 [] = True
    notIn mname1 ((Module mname2 _):rest) =
      if mname1 == mname2
        then False
        else notIn mname1 rest

validateOutput :: Module -> Bool
validateOutput m | undefNT == [] = True
                 | otherwise = error msg
  where
    msg = "Output grammar contains undefined nonterminals: "
          ++ show (undefNTNames undefNT)
    undefNT = undefinedNT m
    undefNTNames [] = []
    undefNTNames ((Nonterminal name):es) = union [name] (undefNTNames es)

-- EOF
