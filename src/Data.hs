module Data where


data Module = Module ModuleName [Statement]
  deriving (Eq, Show)

type ModuleName = String

data Statement
  = Abstract Element
  | Import ModuleName [ImportOption]
  | Override Statement
  | Rule Element [[Element]]
  deriving (Eq, Show)

data Element
  = Nonterminal String
  | Terminal String
  deriving (Eq, Show)

data ImportOption
  = Rename Element Element
  | Drop Element
  deriving (Eq, Show)

data StartPoint = StartPoint Element ModuleName
  deriving (Eq, Show)

type Filename = String
type Argument = String
type Output   = String
type Grammar  = Module
